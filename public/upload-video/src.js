Polymer({
    ready() {
        this.listen(this.$['video-upload'], 'upload-request', 'uploadRequest');
    },
    is: 'upload-video',
    properties: {
        prop1: {
            type: String,
            value: 'upload-video',
        },
        imageURL: {
            type: String,
            value: '../img/video-placeholder.jpg'
        }
    },
    //TODO add the user name
    uploadRequest(e, detail) {
        detail.formData.append('user', this.queryParams.user);
        detail.formData.append('assignment', this.queryParams.assignment);
    }
});
//# sourceMappingURL=src.js.map