import * as express from 'express'
// import * as fileUpload from 'express-fileupload'
import * as serveIndex from 'serve-index'
import * as MediaConverter from 'html5-media-converter'
import * as HTTPStatus from 'http-status'
import * as fs from 'fs'
import * as cors from 'cors'
import * as formidable from 'formidable'
import { spawn } from 'child_process'


const app = express(),
    mc = new MediaConverter({ videoFormats: ['mp4'] })

//setting
const uploadDir = './public/tmp/'
//

app.use('/', express.static('public'))

app.listen(80, () => {
    console.log('running in 80')
})

app.use(function (err, req, res, next) {
    //do logging and user-friendly error message display
    if (err) {
        console.log(err)
        return
    }
})

app.post('/upload', (req, res) => {
    const form = new formidable.IncomingForm()
    form.uploadDir = uploadDir
    form.keepExtensions = true
    form.multiples = false
    form.parse(req, (err, fields, files) => {
        if (err) {
            console.error(err)
            return
        }
        const user = fields.user,
            assignmentId = fields.assignment,
            mimetype = files.sampleFile.type.split('/'),
            extension = [...mimetype].reverse()[0],
            type = [...mimetype][0],
            fileName = `${user}-${assignmentId}-${Date.now()}`,
            fullRenamedPath = `${uploadDir}/${fileName}.${extension}`,
            fullOutputPath = `./public/videos/${fileName}.mp4`

        res.status(HTTPStatus.OK).json({ message: 'converting' })
        fs.rename(files.sampleFile.path, fullRenamedPath, (err) => {
            if (err) {
                console.error(err)
                return
            }
            const ffmpeg = spawn('ffmpeg', [
                '-i', fullRenamedPath,
                '-vf', 'scale=740:-1',
                fullOutputPath
            ])

            ffmpeg.stdout.on('data', data => {
                console.log(`(${fileName}) stdout: ${data}`)
            })

            ffmpeg.stderr.on('data', data => {
                console.log(`(${fileName}) stderr: ${data}`)
            })

            ffmpeg.on('exit', code => {
                console.log(`(${fileName}) Done`);
            });
        })
        form.on('error', function (err) {
            console.error(err)
            return
        })
    })

})

app.get('/getVideo/:userId/:assignmentId', cors(), (req, res) => {
    const userId = req.params.userId,
        assignmentId = req.params.assignmentId
    fs.readdir('./public/videos/', (err, files) => {
        let userFile = files.filter(a => {
            const mask = a.match(/(^\w+)[-](\w+)[-](\w+)(.mp4)/)
            if (mask)
                return mask[1] === userId && mask[2] === assignmentId
            else
                return false
        })
        if (userFile.length <= 0 || err) {
            res.status(HTTPStatus.OK).json({ url: '' })
            return
        }
        userFile.sort((a: String, b: String) => {
            //remove the .mp4 file extension
            let _a = Number(a.match(/(^\w+)[-](\w+)[-](\w+)(.mp4)/)[3]),
                _b = Number(b.match(/(^\w+)[-](\w+)[-](\w+)(.mp4)/)[3])
            return _b - _a
        })
        res.status(HTTPStatus.OK).json({ url: `${req.protocol}://${req.hostname}/videos/${userFile[0]}` })
        return
    })
})
